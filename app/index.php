<?php
session_start();

include('php/__configs.php');

$http_host   = $_SERVER['HTTP_HOST'];
$request_uri = $_SERVER['REQUEST_URI'];
$regex_local = '/192\.168\.1|127\.0\.0|localhost/';

function fileInclude($file){
	$temp   = pathinfo($file);
	echo $file.'?cachetime=' . filemtime($temp['basename']);
}

function getBase(){
	global $http_host;
	global $request_uri;
	global $regex_local;

	$http = @$_SESSION['CONFIGS']['COMPANY']['URL']['ssl'] ? 'https://' : 'http://';
	$www  = substr($http_host, 0, 3) == 'www' ? 'www.' : '';

	if(preg_match($regex_local, $http_host)){
		$joins = [];
		$splitted_url = explode('/', $request_uri);
		foreach($splitted_url as $splits){
			$joins[] = $splits;
			if(preg_match('/dist/', $splits)) break;
		}
		return 'http://' . $http_host . implode('/', $joins) . '/';
	}else{
        if($_SESSION['CONFIGS']['APPROVAL']['active']){
            return $http . $http_host . $request_uri;
        }
		return $http . $http_host .'/';
	}
}

if($_SESSION['CONFIGS']['COMPANY']['URL']['ssl'] && !@$_SERVER['HTTPS'] && !preg_match($regex_local, $http_host)){
	header( 'location: https://' . $http_host . $request_uri );
	die();
}
?>

<!DOCTYPE html>
<html ng-app="app">
<head>

<base href="<?php echo getBase(); ?>" />
<meta name="fragment" content="" />

<title ng-bind="ngMeta.title"><?php echo $_SESSION['CONFIGS']['COMPANY']['name']; ?></title>
<meta content="{{ngMeta.description}}" name="description">

<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
<meta content="pt-br" http-equiv="content-language">
<meta content="<?php echo $_SESSION['CONFIGS']['METATAGS']['theme_color']; ?>" name="theme-color">

<meta content="<?php echo $_SESSION['CONFIGS']['METATAGS']['geo_placename']; ?>" name="geo.placename">
<meta content="<?php echo $_SESSION['CONFIGS']['METATAGS']['geo_region']; ?>" name="geo.region">
<meta content="1 week" name="revisit-after">
<meta content="index, follow" name="robots">
<meta content="index, follow" name="googlebot">

<meta content="Macro Publicidade - 54 3443-4563" name="author">
<meta content="<?php echo $_SESSION['CONFIGS']['COMPANY']['name']; ?>" name="reply-to">

<link ng-bind="seo.canonical" rel="canonical" href="{{ngMeta.canonical}}">
<link rel="alternate" hreflang="pt" href="<?php echo getBase(); ?>">
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

<!-- og -->
<meta content="pt_BR" property="og:locale" />
<meta content="<?php echo getBase(); ?>" property="og:url" />
<meta content="<?php echo $_SESSION['CONFIGS']['COMPANY']['name']; ?>" property="og:title" />
<meta content="<?php echo $_SESSION['CONFIGS']['COMPANY']['name']; ?>" property="og:site_name" />
<meta content="<?php echo $_SESSION['CONFIGS']['COMPANY']['description']; ?>" property="og:description" />
<meta content="images/facebook.png" property="og:image" />
<meta content="image/jpeg" property="og:image:type" />
<meta content="800" property="og:image:width" />
<meta content="600" property="og:image:height" />
<meta content="website" property="og:type" />
<meta content="https://www.facebook.com/" property="article:publisher" />
<meta content="https://www.facebook.com/" property="article:author" />

<!-- twitter -->
<meta content="summary_large_image" name="twitter:card" />
<meta content="<?php echo $_SESSION['CONFIGS']['COMPANY']['name']; ?>" name="twitter:title" />
<meta content="<?php echo $_SESSION['CONFIGS']['COMPANY']['description']; ?>" name="twitter:description" />
<meta content="images/twitter.png" name="twitter:image" />
<meta content="" name="twitter:creator" />
<meta content="" name="twitter:site" />

<link rel="stylesheet" type="text/css" href="<?php fileInclude('css.css')?>">

<!-- htmlmin:ignore -->
<?php
if($_SESSION['CONFIGS']['COMPANY']['KEYS']['google']){
?>
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '<?php echo $_SESSION['CONFIGS']['COMPANY']['KEYS']['google'];?>', 'auto');
ga('require', 'displayfeatures');
</script>
<?php }
if($_SESSION['CONFIGS']['COMPANY']['KEYS']['facebook_pixel']){
?>
<!-- Facebook Pixel Code -->
<script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');fbq('init', '<?php echo $_SESSION['CONFIGS']['COMPANY']['KEYS']['facebook_pixel'];?>');</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $_SESSION['CONFIGS']['COMPANY']['KEYS']['facebook_pixel'];?>&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->
<?php } ?>
<!-- htmlmin:ignore -->
</head>
<body ng-class="{'scroll_blocked': scroll.blocked}">
<ui-view></ui-view>
<!-- js -->
<script type="text/javascript" src="<?php fileInclude('libs.js') ?>"></script>
<script type="text/javascript" src="<?php fileInclude('js.js') ?>"></script>
<script type="text/javascript" src="<?php fileInclude('const.php') ?>"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
