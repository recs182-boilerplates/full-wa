class cProductMulti{
    constructor(Post, $state, $stateParams, $uibModal, $rootScope, $q, $filter){
        this.Post = Post;
        this.state = $state;
        this.stateParams = $stateParams;
        this.uibModal = $uibModal;
        this.rootScope = $rootScope;
        this.q = $q;
        this.filter = $filter;

        const PRODUCT_ID = this.stateParams.id_product || 0;
        const DEFAULT_PRODUCT_IMAGES = [1,2,3,4].map(number => {
            return {id: 0, id_product: PRODUCT_ID, image: '', sort: number};
        });

        this.loaded = false;
        this.indexActiveTab = 0;
        this.data = {
            products: {
                id: PRODUCT_ID,
                reference: '',
                description: '',
                full_description: '',
                specifications: [],
                observation: '',
                active: 1,
            },
            product_images: DEFAULT_PRODUCT_IMAGES,
            product_categories: [],
        };

        this.categories = {
            data: {
                level1: [],
                level2: [],
                level3: [],
            },
            selected: {
                level1: 0,
                level2: 0,
            },
        };

        this.rootScope.$on('UPLOAD_PRODUCT_IMAGE', (event, data) => {
            this.data.product_images[data.index].image = data.image;
        });

        this.init();
    }

    init(){
        this.getCategories(0, 'level1');

        if(this.data.products.id){
            this.Post('products/select-by-id', {id_product: this.data.products.id}).then(res => {
                // parse specifications
                res.products.specifications = JSON.parse(res.products.specifications);

                this.data.products = res.products;

                //images
                res.product_images.forEach((pi, index) => {
                    this.data.product_images[index].id = pi.id;
                    this.data.product_images[index].image = pi.image;
                });

                //categories
                this.data.product_categories = res.product_categories.map(pc => pc.id);
                this.loaded = true;
            }, err => {
                console.log('init', err);
            })
        }else{
            this.loaded = true;
        }
    }

    // create/edit
    multi(){
        const data = copy(this.data);

        if(!data.products.id){
            data.products.creation = new Date().getTime();
        }

        // generate link of description
        let link_treat = this.data.products.description.replace(/ /gi, '-').toLowerCase();
        data.products.link = this.filter('removeAccents')(link_treat);

        // stringify specifications
        data.products.specifications = JSON.stringify(data.products.specifications);

        // filter images
        data.product_images = data.product_images.filter(image => image.image);

        // full description fix
        data.products.full_description = typeof data.products.full_description === 'string' ? data.products.full_description : '';

        this.Post('products/multi', data).then(() => {
            // this.ngToast.create({content: 'Produto criado/editado'});
            this.state.go('main.product.list');
        }, err => {
            // this.ngToast.create({className: 'danger', content: 'Erro ao carregar categorias'});
           console.log('multi: ', err);
        })
    }

    // PRODUCT
    clone(){
        // clear product id
        this.data.products.id = 0;

        // clear ids in images;
        this.data.product_images.forEach(image => {
            image.id = 0;
            image.product_id = 0;
        })
    }

    delete(){
        const confirmation = confirm(`Tem certeza que deseja deletar o produto ${this.data.products.reference}?`);

        if(confirmation){
            this.Post('products/delete', {id_product: this.data.products.id}).then(() => {
                // this.ngToast.create({className: 'info', content: 'Produto removido'});
                this.state.go('main.product.list');
            }, err => {
                // this.ngToast.create({className: 'danger', content: 'Erro ao remover produto'});
                console.log('remove:', err);
            })
        }
    }

    // IMAGES
    uploadImage(index){
        this.uibModal.open({
            templateUrl: 'views/product/multi/upload-image/upload-image.html',
            controller: 'cProductMultiUploadImage',
            controllerAs: '$ctrl',
            resolve: {
                data: () => index
            },
        });
    }

    removeImage(index){
        this.data.product_images[index].id = 0;
        this.data.product_images[index].image = '';
    }

    // CATEGORIES
    getCategories(id = 0, level){
        this.Post('categories/select-by-id', {id_category: id}).then(res => {
            this.categories.data[level] = res;
        }, err => {
            console.log('getCategories: ', err);
            // this.ngToast.create({className: 'danger', content: 'Erro ao carregar categorias'});
        })
    }

    findCategoryLevel(currentLevel, action){
        if(action.match(/plus|mais|\+|next|proximo/)){
            return 'level' + (Number(currentLevel.replace('level', '')) + 1);
        }
        return 'level' + (Number(currentLevel.replace('level', '')) - 1);
    }

    setSelectedCategory(level, id){
        const nextLevel = this.findCategoryLevel(level, 'next');
        if(Number(this.categories.selected[level]) !== Number(id)){
            this.categories.selected[level] = id;

            // get data for selected id
            if(this.categories.data.hasOwnProperty(nextLevel)){
                this.categories.data[nextLevel] = [];
                this.categories.selected[nextLevel] = 0;

                // check if has one more level to clear
                const lastLevel = this.findCategoryLevel(nextLevel, 'next');
                if(this.categories.data.hasOwnProperty(lastLevel)){
                    this.categories.data[lastLevel] = [];
                    this.categories.selected[lastLevel] = 0;
                }

                this.getCategories(id, nextLevel);
            }
        }
    }

    toggleProductCategory(id){
        if(this.data.product_categories.includes(id)){
            const indexOf = this.data.product_categories.indexOf(id);
            this.data.product_categories.splice(indexOf, 1);
        }else{
            this.data.product_categories.push(id);
        }
    }

    checkActiveCategory(id){
        return this.data.product_categories.includes(id);
    }
}