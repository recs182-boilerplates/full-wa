function filterZipCodeBR(){
    return (zipcode) => {
        const formatter = new StringMask('00000-000');
        return formatter.apply(zipcode);
    }
}